program OpenALFpcHello;

{$MODE Delphi}

uses SysUtils, dynlibs, openal;

const

  EXIT_SUCCESS = 0;

var
  helloBuffer, helloSource : ALuint;
  LibALUT: TLibHandle;
  MethodName: string = '';
  libname: string = 'libalut.so';

  alutInit : procedure(argcp: PInteger; argv: PPChar); cdecl;
  alutCreateBufferHelloWorld : function : ALint; cdecl;
  alutSleep : function(duration : ALFloat) : ALBoolean; cdecl;
  alutExit : procedure; cdecl;

  function GetALUTProcAddress(Lib: PtrInt; ProcName: PChar): Pointer;
  begin
    MethodName:=ProcName;
    Result:=GetProcAddress(Lib, ProcName);
  end;

begin

  LibALUT := LoadLibrary(Pchar(libname));
  if LibALUT = 0 then raise Exception.Create('Could not load alut');
  @alutInit := GetALUTProcAddress(LibALUT, 'alutInit');
  @alutCreateBufferHelloWorld := GetALUTProcAddress(LibALUT, 'alutCreateBufferHelloWorld');
  @alutSleep := GetALUTProcAddress(LibALUT, 'alutSleep');
  @alutExit := GetALUTProcAddress(LibALUT, 'alutExit');

  alutInit(@argc, argv);
  helloBuffer := alutCreateBufferHelloWorld;
  alGenSources(1, @helloSource);
  alSourcei(helloSource, AL_BUFFER, helloBuffer);
  alSourcePlay(helloSource);
  alutSleep(1);
  alutExit;
  System.ExitCode := EXIT_SUCCESS;

end.

